# Copyright (C) 2021-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//base/security/huks/build/config.gni")
import("//base/security/huks/huks.gni")
import("//build/ohos.gni")

config("huks_config") {
  include_dirs = [
    "include",
    "../device_cert_manager/include",
  ]
}

if (os_level == "standard") {
  ohos_shared_library("huks_engine_core_standard") {
    subsystem_name = "security"
    part_name = "huks"
    public_configs = [ ":huks_config" ]
    defines = []
    if (huks_use_mbedtls) {
      defines += [ "HKS_USE_MBEDTLS" ]
    }
    include_dirs = [
      "//base/security/huks/utils/crypto_adapter",
      "//base/security/huks/utils/list",
      "//base/security/huks/utils/mutex",
    ]

    sources = [
      "src/hks_auth.c",
      "src/hks_chipset_platform_decrypt.c",
      "src/hks_core_interfaces.c",
      "src/hks_core_service.c",
      "src/hks_core_service_three_stage.c",
      "src/hks_keyblob.c",
      "src/hks_keynode.c",
      "src/hks_secure_access.c",
      "src/hks_sm_import_wrap_key.c",
    ]

    deps = [
      "//base/security/huks/frameworks/huks_standard/main:huks_standard_frameworks",
      "//base/security/huks/services/huks_standard/huks_engine/main/core_dependency:libhuks_core_hal_api_static",
      "//base/security/huks/utils/crypto_adapter:libhuks_utils_client_service_adapter_static",
      "//base/security/huks/utils/file_operator:libhuks_utils_file_operator_static",
      "//base/security/huks/utils/list:libhuks_utils_list_static",
      "//base/security/huks/utils/mutex:libhuks_utils_mutex_static",
    ]

    sanitize = {
      integer_overflow = true
      cfi = true
      debug = false
    }
    sources += [
      "../device_cert_manager/src/dcm_asn1.c",
      "../device_cert_manager/src/dcm_attest.c",
      "../device_cert_manager/src/dcm_attest_utils.c",
    ]

    sources += [ "src/hks_upgrade_key.c" ]

    if (enable_hks_mock) {
      deps += [ "//base/security/huks/services/huks_standard/huks_service/main/systemapi_mock:libhuks_service_systemapi_mock_static" ]
    } else {
      deps += [ "//base/security/huks/services/huks_standard/huks_service/main/systemapi_wrap/useridm:libhuks_service_systemapi_wrap_static" ]
    }

    configs = [
      "//base/security/huks/frameworks/config/build:l2_standard_common_config",
    ]
    external_deps = [ "hiviewdfx_hilog_native:libhilog" ]
  }
} else {
  ohos_shared_library("huks_engine_core_standard") {
    public_configs = [ ":huks_config" ]
    configs = [
      "//base/security/huks/frameworks/config/build:l1_small_common_config",
    ]

    defines = []
    if (huks_use_mbedtls) {
      defines += [ "HKS_USE_MBEDTLS" ]
    }
    include_dirs = [
      "//base/security/huks/utils/crypto_adapter",
      "//base/security/huks/utils/list",
      "//base/security/huks/utils/mutex",
      "//base/security/huks/services/huks_standard/huks_service/main/core/include",
    ]

    sources = [
      "src/hks_auth.c",
      "src/hks_core_interfaces.c",
      "src/hks_core_service.c",
      "src/hks_core_service_three_stage.c",
      "src/hks_keynode.c",
      "src/hks_secure_access.c",
      "src/hks_sm_import_wrap_key.c",
    ]

    sources += [ "src/hks_upgrade_key.c" ]

    if (huks_use_lite_storage == true) {
      sources += [ "src/hks_keyblob_lite.c" ]
    } else {
      sources += [ "src/hks_keyblob.c" ]
    }

    if (huks_use_lite_storage == true) {
      sources += [
        "//base/security/huks/services/huks_standard/huks_service/main/core/src/hks_storage_adapter.c",
        "//base/security/huks/services/huks_standard/huks_service/main/core/src/hks_storage_lite.c",
      ]
    } else {
      sources += [ "//base/security/huks/services/huks_standard/huks_service/main/core/src/hks_storage.c" ]
    }

    deps = [
      "//base/security/huks/frameworks/huks_standard/main:huks_small_frameworks",
      "//base/security/huks/utils/crypto_adapter:libhuks_utils_client_service_adapter_static",
      "//base/security/huks/utils/file_operator:libhuks_utils_file_operator_static",
      "//base/security/huks/utils/list:libhuks_utils_list_static",
      "//base/security/huks/utils/mutex:libhuks_utils_mutex_static",
    ]

    external_deps = [ "hilog_featured_lite:hilog_shared" ]
  }
}
